import axios from "axios";

const RAPID_API_KEY = '11e6908f28mshdacf3ea980f027dp1acac3jsnd2171ae709e4';
const GOOGLE_TRANSLATE_OPTIONS = {
    method: 'POST',
    url: 'https://google-translate1.p.rapidapi.com/language/translate/v2',
    headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'x-rapidapi-host': 'google-translate1.p.rapidapi.com',
        'x-rapidapi-key': RAPID_API_KEY
    }
};

const YANDEX_API_KEY = 'dict.1.1.20181107T091733Z.2a6c7797d50adc31.5d2d532b07fe270031b1e2cfd0812453de7cec90';
const YANDEX_TRANSLATE_OPTIONS = {
    method: 'GET',
    url: 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=' + YANDEX_API_KEY,

};

export default async function translate(sourceLanguage, targetLanguage, text) {
    const WORD_REGEX = /\s/;

    let trimmedText = text.trim();
    let translation = !trimmedText.match(WORD_REGEX) ?
        await translateWord(sourceLanguage, targetLanguage, trimmedText) :
        await translateText(sourceLanguage, targetLanguage, trimmedText);

    return translation;
}

async function translateWord(sourceLanguage, targetLanguage, word) {
    let translations = [];

    let data = { lang: sourceLanguage + '-' + targetLanguage, text: word };
    let options = createYandexApiOptions(YANDEX_TRANSLATE_OPTIONS, data);

    let response = await getTranslationResponse(options);

    for (let pos of response.data.def) {
        translations[pos.pos] = [];
        let posTranslations = pos.tr;
        for (let translation of posTranslations) {
            translations[pos.pos].push(translation.text);
        }
    }

    return translations;
}


async function translateText(sourceLanguage, targetLanguage, text) {
    let data = { source: sourceLanguage, target: targetLanguage, q: text };
    let options = createGoogleApiOptions(GOOGLE_TRANSLATE_OPTIONS, data, sourceLanguage, targetLanguage, text);

    let response = await getTranslationResponse(options);
    let translation = response.data.data.translations[0].translatedText;
    return translation;
}

async function getTranslationResponse(options) {
    try {
        return await axios.request(options);
    } catch (err) {
        console.log(err);
    }
}

function createGoogleApiOptions(optionsTemplate, data) {
    let options = Object.assign(optionsTemplate);
    options.data = encodeData(data);

    return options;

}

function createYandexApiOptions(optionsTemplate, data) {
    let options = Object.assign(optionsTemplate);
    options.url += '&' + encodeData(data);

    return options;

}

function encodeData(data) {
    let formBody = [];
    for (var property in data) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(data[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }

    return formBody.join("&");
}

export class Word {
    constructor(sourceLanguage, targetLanguage, word) {
        this.source = sourceLanguage;
        this.target = targetLanguage;
        this.text = word;
    }

    equals(otherWord) {
        return otherWord.text === this.text &&
            otherWord.source === this.source &&
            otherWord.target === this.target;
    }
}