import { useContext } from "react"
import { TranslationContext } from "../Contexts/TranslationContext"

export default function Dictionary(props) {
    const { context, setContext } = useContext(TranslationContext);

    return (
        <div className="dictionary">
            {props.translations ?
                Object.keys(props.translations).map(
                    pos => (
                        <li key={pos}>
                            {pos}
                            <ol className="translations">
                                {props.translations[pos].map(
                                    translation => (
                                        <li key={translation} onClick={(e) => props.onTranslationClick(e, translation)}>
                                            {translation}
                                        </li>
                                    ))}
                            </ol>
                        </li>
                    ))
                :
                null}
        </div>
    );
}