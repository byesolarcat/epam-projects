import { useContext, useState } from "react"
import { TranslationContext } from "../Contexts/TranslationContext"


export default function LanguageList(props) {
    const { context, setContext } = useContext(TranslationContext);
    const [show, setState] = useState(false);
    const boxType = props.type;

    return (
        <div className="language"
            onClick={() => setState(!show)}>
            <span>{context[boxType].name}</span>
            {show ?
                (<ul className="languages-list">
                    {Object.keys(context.languages).map(lang =>
                        <li
                            key={context.languages[lang]}
                            onClick={(e) => toggleLanguage(e)}>
                            {lang}
                        </li>
                    )}
                </ul>)
                : null ?
                    (<ul className="languages-list">
                        {Object.keys(context.languages).map(lang =>
                            <li
                                key={context.languages[lang]}
                                onClick={(e) => toggleLanguage(e)}>
                                {lang}
                            </li>
                        )}
                    </ul>)
                    : null}
        </div>
    );

    function toggleLanguage(event) {
        event.stopPropagation();
        const selectedLanguage = event.target.innerText.toLowerCase();
        setContext(currentContext => ({
            ...currentContext,
            [boxType]: {
                abbrev: context.languages[selectedLanguage],
                name: selectedLanguage,
                text: context[boxType].text
            }
        }));

        setState(false);
    }
}
