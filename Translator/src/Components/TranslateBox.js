import { useContext, useState } from "react"
import { TranslationContext } from "../Contexts/TranslationContext"
import translate from "../Translator/Translator";
import Dictionary from "./Dictionary";
import LanguageList from "./LanguageList";

export default function TranslateBox(props) {
    const { context, setContext } = useContext(TranslationContext);
    const [{ currentTimer, maxLength }, setState] = useState({
        currentTimer: null,
        maxLength: 1000
    });
    const boxType = props.type;

    return (
        <div className="translate-box" id={boxType} data-language={context[boxType].abbrev}>
            <textarea
                onChange={async (e) => await handleTextAreaInput(e)}
                onKeyPress={async (e) => await handleTextAreaKeyPress(e)}
                value={context[boxType].text}>
            </textarea>
            <LanguageList type={props.type} />
            {boxType === "source" ?
                (<>
                    <div className="clear-button"
                        onClick={(e) => { handleClearClick(e) }}>
                        <i className="far fa-times-circle"></i>
                    </div>
                    <div className="text-length">
                        {context[boxType].text.length}/{maxLength}
                    </div>
                </>)
                :
                (<>
                    <div class="copy-button" onClick={(e) => handleCopyClick(e)}>
                        <i class="far fa-copy" aria-hidden="true"></i>
                    </div>
                    <Dictionary translations={context.translations} onTranslationClick={dictionaryTextClickHandler} />
                </>
                )
            }
        </div>
    )


    async function handleTextAreaInput(event) {
        event.preventDefault();
        clearTimeout(currentTimer);

        if (event.Key !== 'Enter') {
            setContext(currentContext => ({
                ...currentContext,
                [boxType]: {
                    abbrev: context[boxType].abbrev,
                    name: context[boxType].name,
                    text: event.target.value
                }
            }));

            const timer = setTimeout(async () => await initTranslation(), 3000);
            setState(currentState => ({
                ...currentState,
                currentTimer: timer
            }));
        }
    }

    async function handleTextAreaKeyPress(event) {
        clearTimeout(currentTimer);
        if (event.key === "Enter") {
            event.preventDefault();
            event.stopPropagation();
            await initTranslation();
        }
    }

    function handleClearClick(event) {
        event.stopPropagation();

        setContext(currentContext => ({
            ...currentContext,
            source: {
                abbrev: context.source.abbrev,
                name: context.source.name,
                text: ""
            },
            target: {
                abbrev: context.target.abbrev,
                name: context.target.name,
                text: ""
            }
        }));
    }

    function handleCopyClick(event) {
        event.stopPropagation();
        navigator.clipboard.writeText(context.target.text);
    }

    async function initTranslation() {
        try {
            const translated = await translate(context.source.abbrev, context.target.abbrev, context.source.text);
            const firstTranslation = translated[Object.keys(translated)[0]][0];

            setContext(currentContext => ({
                ...currentContext,
                target: {
                    abbrev: context.target.abbrev,
                    name: context.target.name,
                    text: firstTranslation
                },
                translations: translated
            }));
        } catch (err) {
            console.log(err);
            setContext(currentContext => ({
                ...currentContext,
                target: {
                    abbrev: context.target.abbrev,
                    name: context.target.name,
                    text: ""
                },
                translations: null
            }));
        }
    }

    async function dictionaryTextClickHandler(event, text) {
        event.stopPropagation();
        setContext(currentContext => ({
            ...currentContext,
            source: {
                abbrev: currentContext.target.abbrev,
                name: currentContext.target.name,
                text: text
            },
            target: {
                abbrev: currentContext.source.abbrev,
                name: currentContext.source.name,
                text: ""
            },
            translations: null
        }));

        await new Promise(r => setTimeout(r, 100));

        await initTranslation();
    }
}

