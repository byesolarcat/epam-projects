import { useState } from "react";
import TranslateBox from "./Components/TranslateBox";
import { TranslationContext } from "./Contexts/TranslationContext";


function App() {
  const contextValues = {
    source: {
      abbrev: "en",
      name: "English",
      text: ""
    },
    target: {
      abbrev: "ru",
      name: "Russian",
      text: ""
    },
    languages: {
      'english': 'en',
      'french': 'fr',
      'german': "de",
      'russian': 'ru',
      'ukrainian': 'uk'
    },
    translations: null
  };

  const [context, setContext] = useState(contextValues);
  return (
    <div className="text-areas-wrapper">
      <TranslationContext.Provider value={{ context, setContext }}>
        <TranslateBox type="source" />
        <div className="reverse-language" onClick={reverseLanguage}>
          <i className="fas fa-arrows-alt-h" aria-hidden="true"></i>
        </div>
        <TranslateBox type="target" />
      </TranslationContext.Provider>
    </div>
  );


  function reverseLanguage() {
    setContext(currentContext => ({
      ...currentContext,
      source: currentContext.target,
      target: currentContext.source,
      translations: null
    }));
  }
}

export default App;
