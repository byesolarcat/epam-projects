﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using WebFileLoader.EventArgs;

namespace WebFileLoader
{
    public class Downloader : IDisposable
    {
        private bool disposedValue;

        private const int HalfMb = 512 * 1024;
        private const int OneMb = 1024 * 1024;
        private const int TwoMb = 2056 * 1024;
        private const int MaxNumberOfParts = 1000;

        public event EventHandler<DownloadStartedEventArgs> OnDownloadStarted;
        public event EventHandler<DownloadProgressChangedEventArgs> OnDownloadProgressChanged;
        public event EventHandler<DownloadFinishedEventArgs> OnDownloadFinished;

        private readonly HttpClient httpClient = new HttpClient();

        public async Task DownloadFileAsync(string downloadUrl, string fileName)
        {
            using (HttpResponseMessage response = await httpClient.GetAsync(downloadUrl, HttpCompletionOption.ResponseHeadersRead))
            {
                Download download = new Download(fileName, response.Content.Headers.ContentLength.Value, downloadUrl);
                download.Status = DownloadStatus.InProgress;

                try
                {
                    await InitializeDownloadingAsync(response, download);
                    OnDownloadFinished?.Invoke(this, new DownloadFinishedEventArgs(false));
                }
                catch (HttpRequestException)
                {
                    download.Status = DownloadStatus.Error;
                    OnDownloadFinished?.Invoke(this, new DownloadFinishedEventArgs(true));
                }
            }
        }

        private async Task InitializeDownloadingAsync(HttpResponseMessage response, Download download)
        {
            OnDownloadStarted?.Invoke(this, new DownloadStartedEventArgs(download));
            if (response.Headers.AcceptRanges.Contains("bytes"))
            {
                download.PartSize = CalculatePartSize(download.ContentLength);
                await DownloadFileByRangesAsync(download);
            }
            else
            {
                await DownloadWholeFileAsync(download);
            }
        }

        private async Task DownloadFileByRangesAsync(Download download)
        {
            while (download.Status != DownloadStatus.Completed)
            {
                try
                {
                    CancellationTokenSource tokenSource = new CancellationTokenSource();
                    CancellationToken token = tokenSource.Token;

                    List<Task> tasks = new List<Task>();
                    for (int part = 0; part <= download.Parts; part++)
                    {
                        long position = part * download.PartSize;
                        long partEnd = Math.Min(download.ContentLength, position + download.PartSize);
                        tasks.Add(Task.Run(() =>
                            DownloadFileRangeAsync(download, position, partEnd, tokenSource), token
                            ));
                    }

                    await Task.WhenAll(tasks);
                    download.Status = DownloadStatus.Completed;
                }
                catch (HttpRequestException ex)
                {
                    File.Delete(download.FilePath);
                    if (ex.StatusCode is System.Net.HttpStatusCode.TooManyRequests)
                    {
                        const int partSizeModifier = 16;

                        download.PartSize *= partSizeModifier;
                        download.PartsLoaded = 0;
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        private async Task DownloadFileRangeAsync(Download download, long from, long to, CancellationTokenSource tokenSource)
        {
            using (FileStream fileStream = new FileStream(download.FilePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write))
            {
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, download.Url))
                {
                    request.Headers.Range = new RangeHeaderValue(from, to);
                    fileStream.Position = from;
                    using (HttpResponseMessage response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead))
                    {
                        if (((int)response.StatusCode) > 400)
                        {
                            tokenSource.Cancel();
                            throw new HttpRequestException(response.ReasonPhrase, null, response.StatusCode);
                        }

                        await response.Content.CopyToAsync(fileStream);

                        download.PartsLoaded++;
                        OnDownloadProgressChanged?.Invoke(this, new DownloadProgressChangedEventArgs(download));
                    }
                }
            }
        }

        private async Task DownloadWholeFileAsync(Download download)
        {
            using (FileStream fileStream = new FileStream(download.File, FileMode.Create, FileAccess.Write, FileShare.None, OneMb, true))
            {
                using (HttpResponseMessage response = await httpClient.GetAsync(download.Url))
                {
                    await response.Content.CopyToAsync(fileStream);
                    download.PartsLoaded++;
                }
            }
        }

        private static long CalculatePartSize(long contentLength)
        {
            if (contentLength / HalfMb < MaxNumberOfParts)
            {
                return HalfMb;
            }
            else if (contentLength / OneMb < MaxNumberOfParts)
            {
                return OneMb;
            }
            else
            {
                return TwoMb;
            }
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    httpClient.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
