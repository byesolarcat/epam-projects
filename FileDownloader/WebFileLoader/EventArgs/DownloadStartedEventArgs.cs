﻿namespace WebFileLoader.EventArgs
{
    public class DownloadStartedEventArgs
    {
        private const long KiloByte = 1000;
        private const long MegaByte = KiloByte * 1000;
        private const long GigaByte = MegaByte * 1000;

        const string KiloByteString = "Kb";
        const string MegaByteString = "Mb";
        const string GigaByteString = "Gb";

        public Download Download { get; }
        //public string FileName { get; }
        //public string DownloadUrl { get; }
        //public string FileSize { get; }
        //{
        //    get
        //    {
        //        if (contentLength > GigaByte)
        //        {
        //            return string.Concat(contentLength / GigaByte, GigaByteString);
        //        }
        //        else if (contentLength > MegaByte)
        //        {
        //            return string.Concat(contentLength / MegaByte, MegaByteString);
        //        }
        //        else
        //        {
        //            return string.Concat(contentLength / KiloByte, KiloByteString);
        //        }
        //    }
        //}
        //private long contentLength;

        public DownloadStartedEventArgs(Download download)
        {
            //FileName = download.File;
            //DownloadUrl = download.Url.ToString();
            //FileSize = download.FileSize;
            Download = download;
        }

        //public DownloadStartedEventArgs(string fileName, string downloadUrl, long contentLength)
        //{
        //    FileName = fileName;
        //    DownloadUrl = downloadUrl;
        //    this.contentLength = contentLength;
        //}

    }
}