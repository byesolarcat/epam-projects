﻿namespace WebFileLoader.EventArgs
{
    public class DownloadFinishedEventArgs
    {
        public bool WithError { get; }

        public DownloadFinishedEventArgs(bool withError)
        {
            WithError = withError;
        }

    }
}