﻿namespace WebFileLoader.EventArgs
{
    public class DownloadProgressChangedEventArgs
    {
        public string FileName { get; }
        public int Percentage { get; }

        public DownloadProgressChangedEventArgs(Download download)
        {
            FileName = download.File;
            Percentage = (int)download.PartsLoaded;
        }

    }
}