﻿using System;
using System.IO;

namespace WebFileLoader
{
    public class Download
    {
        private const long KiloByte = 1000;
        private const long MegaByte = KiloByte * 1000;
        private const long GigaByte = MegaByte * 1000;

        const string KiloByteString = "Kb";
        const string MegaByteString = "Mb";
        const string GigaByteString = "Gb";

        public string File { get; set; }
        public string FilePath
        {
            get
            {
                string userRoot = Environment.GetEnvironmentVariable("USERPROFILE");
                string savePath = Path.Combine(userRoot, "Downloads\\", File);
                return savePath;
            }
        }

        public DownloadStatus Status { get; set; }
        public string FileSize => GetFormattedSize(ContentLength);
        public Uri Url { get; private set; }
        public int Progress => (int)((float)PartsLoaded / Parts * 100);

        public int Parts => (int)(ContentLength / PartSize);
        public int PartsLoaded { get; set; }
        public long PartSize { get; set; }

        public long ContentLength { get; }

        public Download(string fileName, long contentLength, string url)
        {
            File = fileName;
            ContentLength = contentLength;
            Url = new Uri(url);

            PartsLoaded = 0;
            PartSize = ContentLength;
        }

        public static string GetFormattedSize(long contentLength)
        {
            if (contentLength > GigaByte)
            {
                return string.Concat(contentLength / GigaByte, GigaByteString);
            }
            else if (contentLength > MegaByte)
            {
                return string.Concat(contentLength / MegaByte, MegaByteString);
            }
            else
            {
                return string.Concat(contentLength / KiloByte, KiloByteString);
            }
        }
    }
}
