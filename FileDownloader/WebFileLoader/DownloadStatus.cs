﻿namespace WebFileLoader
{
    public enum DownloadStatus
    {
        InQueue = 0,
        InProgress = 1,
        Completed = 2,
        Error = -1
    }
}
