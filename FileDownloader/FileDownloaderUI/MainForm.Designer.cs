﻿
namespace FileDownloaderUI
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainFormTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.fileInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.fileInfoLayout = new System.Windows.Forms.TableLayoutPanel();
            this.downloadButton = new System.Windows.Forms.Button();
            this.fileFieldsTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.urlFieldLabel = new System.Windows.Forms.Label();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.urlTextBox = new System.Windows.Forms.TextBox();
            this.fileNameTextBox = new System.Windows.Forms.TextBox();
            this.downloadsDataGridView = new System.Windows.Forms.DataGridView();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.mainFormTableLayout.SuspendLayout();
            this.fileInfoGroupBox.SuspendLayout();
            this.fileInfoLayout.SuspendLayout();
            this.fileFieldsTableLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.downloadsDataGridView)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainFormTableLayout
            // 
            resources.ApplyResources(this.mainFormTableLayout, "mainFormTableLayout");
            this.mainFormTableLayout.Controls.Add(this.fileInfoGroupBox, 0, 0);
            this.mainFormTableLayout.Controls.Add(this.downloadsDataGridView, 0, 1);
            this.mainFormTableLayout.Name = "mainFormTableLayout";
            // 
            // fileInfoGroupBox
            // 
            this.fileInfoGroupBox.Controls.Add(this.fileInfoLayout);
            resources.ApplyResources(this.fileInfoGroupBox, "fileInfoGroupBox");
            this.fileInfoGroupBox.Name = "fileInfoGroupBox";
            this.fileInfoGroupBox.TabStop = false;
            // 
            // fileInfoLayout
            // 
            resources.ApplyResources(this.fileInfoLayout, "fileInfoLayout");
            this.fileInfoLayout.Controls.Add(this.downloadButton, 1, 0);
            this.fileInfoLayout.Controls.Add(this.fileFieldsTableLayout, 0, 0);
            this.fileInfoLayout.Name = "fileInfoLayout";
            // 
            // downloadButton
            // 
            resources.ApplyResources(this.downloadButton, "downloadButton");
            this.downloadButton.Name = "downloadButton";
            this.downloadButton.UseVisualStyleBackColor = true;
            this.downloadButton.Click += new System.EventHandler(this.downloadButton_Click);
            // 
            // fileFieldsTableLayout
            // 
            resources.ApplyResources(this.fileFieldsTableLayout, "fileFieldsTableLayout");
            this.fileFieldsTableLayout.Controls.Add(this.urlFieldLabel, 0, 0);
            this.fileFieldsTableLayout.Controls.Add(this.fileNameLabel, 0, 1);
            this.fileFieldsTableLayout.Controls.Add(this.urlTextBox, 1, 0);
            this.fileFieldsTableLayout.Controls.Add(this.fileNameTextBox, 1, 1);
            this.fileFieldsTableLayout.Name = "fileFieldsTableLayout";
            // 
            // urlFieldLabel
            // 
            resources.ApplyResources(this.urlFieldLabel, "urlFieldLabel");
            this.urlFieldLabel.Name = "urlFieldLabel";
            // 
            // fileNameLabel
            // 
            resources.ApplyResources(this.fileNameLabel, "fileNameLabel");
            this.fileNameLabel.Name = "fileNameLabel";
            // 
            // urlTextBox
            // 
            resources.ApplyResources(this.urlTextBox, "urlTextBox");
            this.urlTextBox.Name = "urlTextBox";
            this.urlTextBox.TextChanged += new System.EventHandler(this.urlTextBox_TextChanged);
            // 
            // fileNameTextBox
            // 
            resources.ApplyResources(this.fileNameTextBox, "fileNameTextBox");
            this.fileNameTextBox.Name = "fileNameTextBox";
            this.fileNameTextBox.TextChanged += new System.EventHandler(this.fileNameTextBox_TextChanged);
            // 
            // downloadsDataGridView
            // 
            this.downloadsDataGridView.AllowUserToDeleteRows = false;
            this.downloadsDataGridView.AllowUserToResizeColumns = false;
            this.downloadsDataGridView.AllowUserToResizeRows = false;
            this.downloadsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.downloadsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            resources.ApplyResources(this.downloadsDataGridView, "downloadsDataGridView");
            this.downloadsDataGridView.MultiSelect = false;
            this.downloadsDataGridView.Name = "downloadsDataGridView";
            this.downloadsDataGridView.ReadOnly = true;
            this.downloadsDataGridView.RowHeadersVisible = false;
            this.downloadsDataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.downloadsDataGridView.RowTemplate.Height = 25;
            this.downloadsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.downloadsDataGridView.TabStop = false;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel});
            resources.ApplyResources(this.statusStrip, "statusStrip");
            this.statusStrip.Name = "statusStrip";
            // 
            // toolStripLabel
            // 
            this.toolStripLabel.Name = "toolStripLabel";
            resources.ApplyResources(this.toolStripLabel, "toolStripLabel");
            // 
            // MainForm
            // 
            this.AcceptButton = this.downloadButton;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.mainFormTableLayout);
            this.Name = "MainForm";
            this.mainFormTableLayout.ResumeLayout(false);
            this.fileInfoGroupBox.ResumeLayout(false);
            this.fileInfoLayout.ResumeLayout(false);
            this.fileInfoLayout.PerformLayout();
            this.fileFieldsTableLayout.ResumeLayout(false);
            this.fileFieldsTableLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.downloadsDataGridView)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainFormTableLayout;
        private System.Windows.Forms.GroupBox fileInfoGroupBox;
        private System.Windows.Forms.TableLayoutPanel fileInfoLayout;
        private System.Windows.Forms.Button downloadButton;
        private System.Windows.Forms.TableLayoutPanel fileFieldsTableLayout;
        private System.Windows.Forms.Label urlFieldLabel;
        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.TextBox urlTextBox;
        private System.Windows.Forms.TextBox fileNameTextBox;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.DataGridView downloadsDataGridView;
        private System.Windows.Forms.ToolStripStatusLabel toolStripLabel;
    }
}

