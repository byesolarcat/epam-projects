﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FileDownloaderUI.DataGridViewProgressBar
{
    class DataGridViewProgressCell : DataGridViewCell
    {
        static Color ProgressBarColor => Color.Green;

        private int width;


        public DataGridViewProgressCell()
        {
            this.ValueType = typeof(int);
            Value = 0;
        }

        protected override void Paint(Graphics g,
            Rectangle clipBounds,
            Rectangle cellBounds,
            int rowIndex,
            DataGridViewElementStates cellState,
            object value, object formattedValue,
            string errorText,
            DataGridViewCellStyle cellStyle,
            DataGridViewAdvancedBorderStyle advancedBorderStyle,
            DataGridViewPaintParts paintParts)
        {
            Rectangle progressBarArea = cellBounds;
            Rectangle progressBarAdjustment = this.BorderWidths(advancedBorderStyle);

            progressBarArea.X += progressBarAdjustment.X;
            progressBarArea.Y += progressBarAdjustment.Y;

            progressBarArea.Height += progressBarAdjustment.Height - 2;
            progressBarArea.Width += progressBarAdjustment.Width;

            int progress = (int)DataGridView[ColumnIndex, rowIndex].Value;
            if (progress < 0)
                progress = 100;

            width = (int)(((double)progressBarArea.Width / 100) * progress);

            Rectangle progressBar = new Rectangle(progressBarArea.Location, new Size(width, progressBarArea.Height));

            g.FillRectangle(new SolidBrush(ProgressBarColor), progressBar);
        }
    }
}
