﻿using System.Windows.Forms;

namespace FileDownloaderUI.DataGridViewProgressBar
{
    class DataGridViewProgressColumn : DataGridViewColumn
    {
        public DataGridViewProgressColumn()
        {
            this.CellTemplate = new DataGridViewProgressCell();
        }
    }
}
