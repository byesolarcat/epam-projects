﻿using FileDownloaderUI.DataGridViewProgressBar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using WebFileLoader;
using WebFileLoader.EventArgs;

namespace FileDownloaderUI
{
    public partial class MainForm : Form
    {
        private const string statusStripFormat = "Total: {0} Completed: {1} ({2}) In Progress: {3} In Queue: {4} Errors: {5}";

        private Downloader downloader = new Downloader();
        private BindingList<Download> downloads = new BindingList<Download>();

        public MainForm()
        {
            InitializeComponent();

            InitializeDataGridColumns();

            SubscribeToDownloaderEvents();
            RefreshStatusStripText();
        }

        private async void downloadButton_Click(object sender, EventArgs e)
        {
            string url = urlTextBox.Text;
            string fileName = fileNameTextBox.Text;

            ClearUserInputControls();

            await downloader.DownloadFileAsync(url, fileName);
        }

        private void urlTextBox_TextChanged(object sender, EventArgs e)
        {
            string nameFromUrl = GetFileNameFromUrlTextBox();
            SetFileNameControlText(nameFromUrl);
            SetDownloadButtonState();
        }

        private void fileNameTextBox_TextChanged(object sender, EventArgs e)
        {
            SetDownloadButtonState();
        }

        private void InitializeDataGridColumns()
        {
            downloadsDataGridView.Enabled = false;

            downloadsDataGridView.Columns.AddRange(
                new DataGridViewColumn(new DataGridViewTextBoxCell()),
                new DataGridViewColumn(new DataGridViewTextBoxCell()),
                new DataGridViewColumn(new DataGridViewTextBoxCell()),
                new DataGridViewProgressColumn(),
                new DataGridViewColumn(new DataGridViewTextBoxCell()));

            downloadsDataGridView.Columns[0].Name = "File";
            downloadsDataGridView.Columns[0].DataPropertyName = "File";

            downloadsDataGridView.Columns[1].Name = "Status";
            downloadsDataGridView.Columns[1].DataPropertyName = "Status";

            downloadsDataGridView.Columns[2].Name = "FileSize";
            downloadsDataGridView.Columns[2].DataPropertyName = "FileSize";

            downloadsDataGridView.Columns[3].Name = "Progress";
            downloadsDataGridView.Columns[3].DataPropertyName = "Progress";

            downloadsDataGridView.Columns[4].Name = "Url";
            downloadsDataGridView.Columns[4].DataPropertyName = "Url";

            downloadsDataGridView.AutoGenerateColumns = false;
            downloadsDataGridView.DataSource = downloads;
        }

        private void SubscribeToDownloaderEvents()
        {
            downloader.OnDownloadStarted +=
                 delegate (object sender, DownloadStartedEventArgs e)
                 {
                     downloads.Add(e.Download);
                     RefreshDataGridView();
                     RefreshStatusStripText();
                 };

            downloader.OnDownloadProgressChanged +=
                delegate (object sender, DownloadProgressChangedEventArgs e)
                {
                    downloadsDataGridView.Invalidate();
                    RefreshStatusStripText();
                };

            downloader.OnDownloadFinished +=
                delegate (object sender, DownloadFinishedEventArgs e)
                {
                    downloadsDataGridView.Invalidate();
                    RefreshStatusStripText();
                };
        }

        private void RefreshDataGridView()
        {
            downloadsDataGridView.DataSource = null;
            downloadsDataGridView.DataSource = downloads;
        }

        private void ClearUserInputControls()
        {
            urlTextBox.Clear();
            fileNameTextBox.Clear();
        }

        private string GetFileNameFromUrlTextBox()
        {
            if (Uri.TryCreate(urlTextBox.Text, UriKind.Absolute, out Uri result))
            {
                return Path.GetFileName(result.AbsolutePath);
            }

            return string.Empty;
        }

        private void SetFileNameControlText(string text)
        {
            fileNameTextBox.Text = text;
        }

        private void SetDownloadButtonState()
        {
            downloadButton.Enabled = !string.IsNullOrWhiteSpace(fileNameTextBox.Text) &&
                                     Uri.TryCreate(urlTextBox.Text, UriKind.RelativeOrAbsolute, out Uri result);
        }

        private void RefreshStatusStripText()
        {
            int total = downloads.Count;

            int completed = CountDownloadsWithStatus(DownloadStatus.Completed);
            string downloaded = GetDownloadedSize();

            int inProgress = CountDownloadsWithStatus(DownloadStatus.InProgress);
            int inQueue = CountDownloadsWithStatus(DownloadStatus.InQueue);
            int errors = CountDownloadsWithStatus(DownloadStatus.Error);
            toolStripLabel.Text = string.Format(statusStripFormat,
                                             total, completed, downloaded, inProgress, inQueue, errors);
        }

        private string GetDownloadedSize()
        {
            long contentLengthSum = downloads.Where(d => d.Status == DownloadStatus.Completed).Sum(d => d.ContentLength);
            return Download.GetFormattedSize(contentLengthSum);
        }

        private int CountDownloadsWithStatus(DownloadStatus status)
        {
            return downloads.Where(d => d.Status == status).Count();
        }
    }
}
