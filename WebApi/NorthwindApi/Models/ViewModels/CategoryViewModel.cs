﻿namespace NorthwindApi.Models
{
    public class CategoryViewModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }

        public CategoryViewModel(Category category)
        {
            CategoryId = category.CategoryId;
            CategoryName = category.CategoryName;
            Description = category.Description;

            Picture = category.Picture == null ? 
                string.Empty :
                string.Format("/api/Categories/{0}/Image", CategoryId);
        }

        public Category GetCategoryEntity()
        {
            Category category = new Category();
            category.CategoryId = CategoryId;
            category.CategoryName = CategoryName;
            category.Description = Description;

            return category;
        }
    }
}
