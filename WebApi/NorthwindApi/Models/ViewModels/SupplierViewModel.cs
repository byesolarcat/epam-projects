﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindApi.Models
{
    public class SupplierViewModel
    {
        public int SupplierId { get; set; }
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string HomePage { get; set; }

        public SupplierViewModel(Supplier supplier)
        {
            SupplierId = supplier.SupplierId;
            CompanyName = supplier.CompanyName;
            ContactName = supplier.ContactName;
            ContactTitle = supplier.ContactTitle;
            Address = supplier.Address;
            City = supplier.City;
            Region = supplier.Region;
            PostalCode = supplier.PostalCode;
            Country = supplier.Country;
            Phone = supplier.Phone;
            Fax = supplier.Fax;
            HomePage = supplier.HomePage;
        }

        public Supplier GetSupplierEntity()
        {
            Supplier supplier = new Supplier();

            supplier.SupplierId = SupplierId;
            supplier.CompanyName = CompanyName;
            supplier.ContactName = ContactName;
            supplier.ContactTitle = ContactTitle;
            supplier.Address = Address;
            supplier.City = City;
            supplier.Region = Region;
            supplier.PostalCode = PostalCode;
            supplier.Country = Country;
            supplier.Phone = Phone;
            supplier.Fax = Fax;
            supplier.HomePage = HomePage;

            return supplier;
        }
    }
}
