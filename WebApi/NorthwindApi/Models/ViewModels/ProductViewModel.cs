﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindApi.Models
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        
        private int? supplierId;
        public string Supplier { get; set; }

        private int? categoryId;
        public string Category { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal? UnitPrice { get; set; }
        public short? UnitsInStock { get; set; }
        public short? UnitsOnOrder { get; set; }
        public short? ReorderLevel { get; set; }
        public bool Discontinued { get; set; }

        public ProductViewModel(Product product)
        {
            ProductId = product.ProductId;
            ProductName = product.ProductName;

            supplierId = product.SupplierId;
            Supplier = supplierId.HasValue ? product.Supplier.CompanyName : null;

            categoryId = product.CategoryId;
            Category = categoryId.HasValue ? product.Category.CategoryName : null;

            QuantityPerUnit = product.QuantityPerUnit;
            UnitPrice = product.UnitPrice;
            UnitsInStock = product.UnitsInStock;
            UnitsOnOrder = product.UnitsOnOrder;
            ReorderLevel = product.ReorderLevel;
            Discontinued = product.Discontinued;
        }

        public Product GetProductEntity()
        {
            Product product = new Product();

            product.ProductId = product.ProductId;
            product.ProductName = product.ProductName;

            product.SupplierId = supplierId;
            product.CategoryId = categoryId;

            product.QuantityPerUnit = QuantityPerUnit;
            product.UnitPrice = UnitPrice;
            product.UnitsInStock = UnitsInStock;
            product.UnitsOnOrder = UnitsOnOrder;
            product.ReorderLevel = ReorderLevel;
            product.Discontinued = Discontinued;

            return product;
        }
    }
}
