﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NorthwindApi.Models;

namespace NorthwindApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly NorthwindContext _context;

        public CategoriesController(NorthwindContext context)
        {
            _context = context;
        }

        // GET: api/Categories
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryViewModel>>> GetCategories()
        {
            return await _context.Categories.Select(c => new CategoryViewModel(c)).ToListAsync();
        }

        // GET: api/Categories/5
        [HttpGet("{id:int}")]
        public async Task<ActionResult<CategoryViewModel>> GetCategory(int id)
        {
            var category = await _context.Categories.FindAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            return new CategoryViewModel(category);
        }

        // GET: api/Categories/5/Image
        [HttpGet("{id:int}/Image")]
        public async Task<ActionResult<CategoryViewModel>> GetCategoryImage(int id)
        {
            const string GarbageData = "FRwvAAIAAAANAA4AFAAhAP";
            var category = await _context.Categories.FindAsync(id);

            if (category == null || category.Picture == null)
            {
                return NotFound();
            }

            var b64data = Convert.ToBase64String(category.Picture);
            if (b64data.StartsWith(GarbageData))
            {
                byte[] notGarbagePictureData = category.Picture.Skip(78).ToArray();
                return File(notGarbagePictureData, "image/bmp");
            }

            return File(category.Picture, "image/bmp");
        }

        // PUT: api/Categories/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutCategory(int id, CategoryViewModel categoryViewModel)
        {
            if (id != categoryViewModel.CategoryId)
            {
                return BadRequest();
            }

            var category = await _context.Categories.Where(c => c.CategoryId == id).FirstAsync();
            category.CategoryName = categoryViewModel.CategoryName;
            category.Description = categoryViewModel.Description;

            _context.Entry(category).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // PUT: api/Categories/5/Image
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id:int}/Image")]
        public async Task<IActionResult> PutCategoryImage(int id, [FromForm(Name = "picture")] IFormFile picture)
        {
            var category = await _context.Categories.Where(c => c.CategoryId == id).FirstAsync().ConfigureAwait(false);

            if (category == null)
            {
                return BadRequest();
            }

            if (picture == null)
            {
                category.Picture = null;
            }
            else
            {
                category.Picture = new byte[picture.Length];
                var stream = picture.OpenReadStream();
                await using (stream.ConfigureAwait(false))
                {
                    await stream.ReadAsync(category.Picture.AsMemory(0, category.Picture.Length)).ConfigureAwait(false);
                }
            }

            _context.Entry(category).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }

                throw;
            }

            return NoContent();
        }

        // POST: api/Categories
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Category>> PostCategory(CategoryViewModel categoryViewModel)
        {
            var category = categoryViewModel.GetCategoryEntity();
            _context.Categories.Add(category);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCategory", new { id = category.CategoryId }, category);
        }

        // DELETE: api/Categories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            var category = await _context.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            _context.Categories.Remove(category);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CategoryExists(int id)
        {
            return _context.Categories.Any(e => e.CategoryId == id);
        }
    }
}
