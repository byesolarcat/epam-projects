# ASP.NET Core REST API

REST API для продуктов и категорий из учебной базы данных Northwind.

## Технологии

* C#
* .NET Core 5
* ASP.NET Core
* Entity Framework Core
* Swagger-Documentation
* Multithreading (TPL)
* MSSQL

## Скриншоты

### Swagger-Doc

![Swagger-doc](https://i.imgur.com/ktaKZbT.png, "Swagger-doc")